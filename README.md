Usage:
    
    python ChangeColor.py [byte default=210]
    
    
This program will convert all pixels with an average RGB value above the parameter passed to white, and anything else to black. 

A parameter greater than 255 or less than 0 won't cause error, although it's pointless because the image will be all white or all black.

All files in the directory the script is run from are used, and a file of the same name + 'NEW.BMP" is created. Original files are not changed.