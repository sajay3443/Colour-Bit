import sys
import pygame
import os

if len(sys.argv) > 1:
    Filter = int(sys.argv[1])*3
else:
    Filter = 210*3

os.chdir(os.getcwd())
for I in os.listdir(os.getcwd()):
    try:
        Pic = pygame.image.load(I)
        G = Pic.get_size()
        for W in range(0, G[0]):
            for L in range(0, G[1]):
                Pix = Pic.get_at((W, L))
                if Pix[0] + Pix[1] + Pix[2] > Filter:
                    Pic.set_at((W, L), (255,255,255))
                else:
                    Pic.set_at((W, L), (0,0,0))
                            
        pygame.image.save(Pic, (I + "NEW.BMP"))
    except pygame.error:
        pass
